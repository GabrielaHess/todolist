$(function () {
    var ul = $('ul');
    var listUrl = 'http://localhost:3000/todolist';
    // console.log(url);

    function insertToDoList(todolist) {
        $.each(todolist, function (i, toDo) {
            var itemId = toDo.id;
            var li = $('<li data-id="' + itemId + '"></li>');
            var checkBox = $('<input class="checkBox" type="checkbox"></input>');
            var checkBoxProp = toDo.done;
            if (checkBoxProp === 'true') {
                checkBox.prop('checked', true);
                li.toggleClass('completed');
            }
            else {
                checkBox.prop('checked', false);
                li.toggleClass('active');
            }
            var toDoItem = $('<span class="col-4"></span>').text(toDo.item);
            var remButton = $('<button class="remButton">Remove</button>')
            li.append(checkBox).append(toDoItem).append(remButton);
            ul.append(li);
        });
        updateItemsLeft();
        sortList();
    };

    function loadToDoList() {
        $.ajax({
            url: listUrl,
            dataType: 'json'
        }).done(function (response) {
            insertToDoList(response);
        }).fail(function (error) {
            console.log(error);
        });
    };
    loadToDoList();

    var form = $('form');
    var addButton = $('.inputButton');
    form.on('submit', function (e) {
        e.preventDefault();
        var newToDo = $('.inputBox').val();
        // console.log('aa');
        var data = {
            item: newToDo,
            done: false,
        };
        addNewToDo(data);
    });
    function addNewToDo(data) {
        $.ajax({
            type: 'POST',
            url: listUrl,
            data: data
        }).done(function (response) {
            insertToDoList([response]);
            $('.inputBox').val('');
            updateItemsLeft();
        }).fail(function (error) {
            console.log(error);
        });
    };

    ul.on('click', '.remButton', function () {
        // console.log();

        var itemId = $(this).parent().data('id');
        var itemUrl = listUrl + '/' + itemId;
        $.ajax({
            type: 'DELETE',
            url: itemUrl
        }).done(function () {
            $(this).parent().remove();
            updateItemsLeft();
        }.bind(this)).fail(function (error) {
            console.log(error);
        });
    });


    // var checkBoxes = $('li').find('input');
    ul.on('click', '.checkBox', function () {
        var item = $(this).parent().find('span').text();
        var itemId = $(this).parent().data('id');
        var itemUrl = listUrl + '/' + itemId;
        var itemDone = $(this).prop('checked');
        console.log($(this))
        if ($(this).prop('checked') === true) {
            // $(this).prop('checked', true);
            $(this).parent().addClass('completed').removeClass('active');
        }
        if ($(this).prop('checked') === false) {
            // $(this).prop('checked', false);
            $(this).parent().removeClass('completed').addClass('active');
        }
        if (itemDone === true) {
            var data = {
                item: item,
                done: true
            }
            changeDone(data, itemUrl);
        }
        if (itemDone === false) {
            var data = {
                item: item,
                done: false
            }
            changeDone(data, itemUrl);
        }
        sortList();
    });
    function changeDone(data, itemUrl) {
        $.ajax({
            type: 'PUT',
            url: itemUrl,
            data
        }).done(function () {
            updateItemsLeft();
        }).fail(function (error) {
            console.log(error);
        });
    }
    function updateItemsLeft() {
        var itemsList = $('li');
        var items = 0;
        // console.log(itemsList);
        itemsList.each(function () {
            if ($(this).find('.checkBox').prop('checked') === false) {
                items++;
            }
            // console.log(items);
        });
        // console.log(items);
        if (items === 0) {
            $('.itemLeft').text('Everything is done')
        };
        if (items === 1) {
            $('.itemLeft').text(items + ' item left')
        }
        if (items > 1) {
            $('.itemLeft').text(items + ' items left')
        }
    };
    var buttonClearCompleted = $('#clearCompleteButton');
    var buttonAll = $('#allButton');
    var buttonActive = $('#activeButton');
    var buttonCompleted = $('#completeButton');

    buttonClearCompleted.on('click', function () {
        var clean = ul.find('.completed');
        // console.log(clean);
        clean.each(function () {
            var itemId = $(this).data('id');
            var itemUrl = listUrl + '/' + itemId;
            $.ajax({
                type: 'DELETE',
                url: itemUrl
            }).done(function () {
                $(this).remove();
                updateItemsLeft();
            }.bind(this)).fail(function (error) {
                console.log(error);
            });
        })
        // var items = ul.children().data('id');
    });
    buttonAll.on('click', function () {
        ul.removeClass('active completed')
        ul.children().show();
    });
    buttonActive.on('click', function () {
        ul.addClass('active').removeClass('completed');
        sortList()
    });
    buttonCompleted.on('click', function () {
        ul.addClass('completed').removeClass('active');
        sortList()
    });
    function sortList() {
        if (ul.hasClass('active')) {
            ul.children().each(function () {
                if ($(this).hasClass('active')) {
                    $(this).show()
                }
                if ($(this).hasClass('completed')) {
                    $(this).hide()
                }
            });
        };
        if (ul.hasClass('completed')) {
            ul.children().each(function () {
                if ($(this).hasClass('completed')) {
                    $(this).show();
                }
                if ($(this).hasClass('active')) {
                    $(this).hide();
                }
            });

        };
    };




});