var gulp = require('gulp');
var nunjucksRender = require('gulp-nunjucks-render');
var connect = require('gulp-connect');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('webserver', function () {
    connect.server({
        root: '',
        port: 8080,
        livereload: true
    });
});

gulp.task('reload', ['nunjucks'], function () {
    return gulp.src('')
        .pipe(connect.reload());
});

gulp.task('nunjucks', function () {
    nunjucksRender.nunjucks.configure(['templates/'], {watch: false});

    return gulp.src('templates/pages/*.html')
        .pipe(nunjucksRender())
        .pipe(gulp.dest('.'));
});

gulp.task('pure_reload', function () {
    gulp.src('.')
        .pipe(connect.reload());
});

gulp.task('watch', function () {
    gulp.watch(['index.html'], ['reload']);
    gulp.watch(['media/images/*'], ['pure_reload']);
    gulp.watch(['styles/*.scss'], ['sass']);
    gulp.watch(['scripts/*.js'], ['pure_reload']);
    gulp.watch(['styles/*.css'], ['pure_reload']);

});

gulp.task('sass', function() {
  return gulp.src('styles/style.scss')
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(sourcemaps.write())
      .pipe(autoprefixer('last 10 versions', 'ie 9'))
      .pipe(gulp.dest('styles/'))
      .pipe(connect.reload());
});


gulp.task('default', ['nunjucks', 'watch', 'webserver', 'sass'], function () {

});
